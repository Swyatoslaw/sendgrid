'use strict';

/**
 * @ngdoc function
 * @name sendGridApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sendGridApp
 */
angular.module('sendGridApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
