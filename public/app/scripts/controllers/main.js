'use strict';

/**
 * @ngdoc function
 * @name sendGridApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sendGridApp
 */
angular.module('sendGridApp')
  .controller('MainCtrl', function ($http, $scope, $timeout) {
    $scope.message = {count: 1};
    $scope.viewMessage = false;
    $scope.newTemplate = {
      name: undefined
    }

    $scope.version = {active: 1, name:"version1", subject: "<%subject%>", html_content: "<%body%>", plain_content: ""}

    var SEND_KEY = "Bearer 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"

    $scope.sendEmail = function(id) {
      $scope.message.id = id;
      $http({
        data: $scope.message,
        method: 'POST',
        url: 'http://localhost:1337/main/index',
      }).then(function successCallback(response) {
          var result = response.data;
          if (angular.isDefined(result.message)) {
            $scope.viewStatus = true;
            $scope.status = result.message +" Work time: " + result.time+" miliseconds";

            $timeout(function() {
              $scope.viewStatus = false;
              $scope.status = "";
              $scope.futureEmailData = {};
              $scope.message = {};
              $scope.viewMessage = false;
            },5000);
          }
      }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }

    $scope.setIntoMail = function(index) {
      $scope.getTemplate(index);
      $scope.viewMessage = true;
    }

    $scope.getTemplate = function(index) {
      $http({
        method: 'GET',
        url: 'https://api.sendgrid.com/v3/templates/'+$scope.templates[index].id,
        headers: {
         'Content-Type': 'application/json',
         'Authorization': SEND_KEY
        },
      }).then(function successCallback(response) {
        $scope.futureEmailData = response.data;
      }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }


    $scope.getTemplates = function() {
      $http({
        method: 'GET',
        url: 'https://api.sendgrid.com/v3/templates',
        headers: {
         'Content-Type': 'application/json',
         'Authorization': SEND_KEY
        },
      }).then(function successCallback(response) {
        $scope.templates = response.data.templates;
      }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }

    $scope.getTemplates();

    $scope.removeTemplate = function(index) {
      $http({
        method: 'DELETE',
        url: 'https://api.sendgrid.com/v3/templates/'+$scope.templates[index].id,
        headers: {
         'Content-Type': 'application/json',
         'Authorization': SEND_KEY
        },
      }).then(function successCallback(response) {
        // this callback will be called asynchronously
        // when the response is available
      }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
      $scope.templates.splice(index,1);
    };


    $scope.createNewTemplate = function() {
      $http({
        data: {
          name: $scope.newTemplate.name
        },
        method: 'POST',
        url: 'https://api.sendgrid.com/v3/templates',
        headers: {
         'Content-Type': 'application/json',
         'Authorization': SEND_KEY
        },
      }).then(function successCallback(response) {

          $http({
            method: 'POST',
            url: 'https://api.sendgrid.com/v3/templates/'+response.data.id+'/versions',
            headers: {
             'Content-Type': 'application/json',
             'Authorization': SEND_KEY
            },
            data: $scope.version,
          }).then(function successCallback(version) {
            response.data.versions = [version.data];
            $scope.templates.push(response.data)
            $scope.newTemplate = {};
          }, function errorCallback(response) {
          });
      }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }


  });
