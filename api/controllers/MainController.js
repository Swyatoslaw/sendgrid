/**
 * MainController
 *
 * @description :: Server-side logic for managing mains
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index: function (req, res) {
    var start = new Date().getTime();
    async.parallel({
      result: function (callback) {
        return SendGridService.sendMail(callback, req.body);
      }
    }, function (err, results) {
      var end = new Date().getTime();
      var time = end - start;
      return res.json({status: 'success', message: "Messages sent successfully!", time: time});
    })
  }

};

