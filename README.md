# sendGrid

a [Sails](http://sailsjs.org) application

## Overview

sendGrid project requires:

* [JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html) - 1.7.x
* [Grails](https://grails.org/) - 1.3.7
* [SDKMAN](http://sdkman.io/) - ~5.5.0
* [Oracle DB](http://www.oracle.com/technetwork/database/enterprise-edition/downloads/112010-linx8664soft-100572.html) - 11g

## Run project

```sh
$ sudo npm -g install sails
```

Run inside project folder

```sh
$ sails lift
```

Open in browser:  [SendGrid application](http://localhost:1337/)
